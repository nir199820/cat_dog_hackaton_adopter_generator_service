from graphql_handler.graphqlHandler import GraphQlMutation
from generations.generateAdopters import generate_adopters
from Data.app_properties import JAVA_server_url

import time


def create_adopter():
    while True:
        graph_mutation_handler = GraphQlMutation(JAVA_server_url)
        adopter = generate_adopters(1)[0]
        graph_mutation_handler.set_adopter(adopter)
        time.sleep(3)
