from requests_handler import create_adopter
from Data.app_properties import port
from flask import Flask, request
import threading


adopter_creator = threading.Thread(target=create_adopter, daemon=True)

threads = dict(adopter_creator=adopter_creator)

app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def hello():
    if request.method == 'GET':
        return "Hello _GET"
    elif request.method == 'POST':
        return "Hello _POST"
    else:
        return None


if __name__ == '__main__':
    print("creating adopters")
    threads['adopter_creator'].start()
    print("starting app")
    app.run("0.0.0.0", port)
    threads['adopter_creator'].join()


