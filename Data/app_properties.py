from generations.generateHeatmap import generate_heat_map

JAVA_server_port = '9001'
JAVA_server_ip = 'http://129.213.158.42'
JAVA_server_url = JAVA_server_ip+':'+JAVA_server_port+'/graphql'
grid_distribution = generate_heat_map()
port = 5000
